#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Desplazar la última línea del programa anterior hacia arriba.
"""Al colocar la llamada a la función al inicio no nos permitirá correr el progrma y obtendremos un NameError
indicado que no está definida"""

repite_estribillo()
def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')
def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()