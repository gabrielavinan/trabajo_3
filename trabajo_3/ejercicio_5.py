#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
# ¿Qué mostrará en pantalla el siguiente programa Python?

def fred():
    print("Zap")
def jane():
    print("ABC")
jane()
fred()
jane()

# a) Zap ABC jane fred jane
# b) Zap ABC Zap
# c) ABC Zap jane
# d) ABC Zap ABC-----> CORRECTA
# e) Zap Zap Zap