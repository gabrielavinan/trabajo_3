#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Reescribe el programa de cálculo del salario, con tarifa-ymedia para las horas extras,
# y crea una función llamada calculo_salario que reciba dos parámetros (horas y tarifa).

horas= int(input("Ingrese el numero de horas: "))
tarifa= float(input("Ingrese la tarifa: "))

def calculo_salario (horas, tarifa):
    salario = float(horas * tarifa)
    if horas > 40:
         horasextra= horas-40
         valorextra= (tarifa*1.5) * horasextra
         salario = (horas - horasextra) * tarifa
         total=valorextra+salario
         print("El valor del salario con horas extra es: ", total)
    elif horas <=40:
         print("El valor del salario es:", salario)
(calculo_salario(horas,tarifa))