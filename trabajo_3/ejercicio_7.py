#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Reescribe el programa de calificaciones del capítulo anterior usando una función llamada
# calcula_calificacion, que reciba una puntuación como parámetro y devuelva una calificación como cadena.

puntuacion=float(input("Ingrese la puntuación:"))
def calcula_calificacion(puntuacion):
    if puntuacion >= 0 and puntuacion <= 1.0:
        if puntuacion >= 0.9:
            print("Sobresaliente")
        elif puntuacion >= 0.8:
            print("Notable")
        elif puntuacion >= 0.7:
            print("Bien")
        elif puntuacion >= 0.6:
            print("Suficiente")
        elif puntuacion < 0.6:
            print("Inuficiente")
    else:
        print("Putuacion incorrecta")
calcula_calificacion(puntuacion)
