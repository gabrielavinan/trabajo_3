#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Desplaza la llamada de la función de nuevo hacia el final,
#y coloca la definición de muestra_estribillo después de la definición de repite_estribillo.

def  repite_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')
def muestra_estribillo():
    muestra_estribillo()
    muestra_estribillo()
repite_estribillo()